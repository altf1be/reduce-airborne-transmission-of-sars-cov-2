# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.1.5](https://bitbucket.org/altf1be/promcf/compare/v0.1.4...v0.1.5) (2021-01-30)


### Documentations

* **fix:** remove :term: inside bold text because it is not interpreted inside a PDF ([ac5ec80](https://bitbucket.org/altf1be/promcf/commit/ac5ec80b3fe56ce4ea172076d57cc4a8e3bbc1e8))

### [0.1.4](https://bitbucket.org/altf1be/promcf/compare/v0.1.3...v0.1.4) (2021-01-30)


### Documentations

* **fix:** remove the Module Index ([466dcfc](https://bitbucket.org/altf1be/promcf/commit/466dcfc5f874dd61c7156f860e0bcf50b3e585cb))

### [0.1.3](https://bitbucket.org/altf1be/promcf/compare/v0.1.2...v0.1.3) (2021-01-30)


### Continous improvements (CI)

* add docxbuilder in requirements.txt ([98bfade](https://bitbucket.org/altf1be/promcf/commit/98bfade52356c7a13580b6300fe2c9f5c916c234))

### [0.1.2](https://bitbucket.org/altf1be/promcf/compare/v0.1.1...v0.1.2) (2021-01-30)


### Continous improvements (CI)

* add sphinxcontrib-bibtex and upgrade Unidecode in requirements.txt ([bc10551](https://bitbucket.org/altf1be/promcf/commit/bc10551813e66b9ab218e864d0d45aaa5505e658))

### [0.1.1](https://bitbucket.org/altf1be/promcf/compare/v0.1.0...v0.1.1) (2021-01-30)


### Continous improvements (CI)

* add unidecode in requirements.txt ([7c282bb](https://bitbucket.org/altf1be/promcf/commit/7c282bb9e0a88d7a343ece969e3a725021484c5d))

## 0.1.0 (2021-01-30)


### Documentations

* create the standalone version of the document named 'The Role Of Relative Humidity In Airborne Transmission Of Sars-Cov-2 In Indoor Conditions' ([966c618](https://bitbucket.org/altf1be/promcf/commit/966c618538e0b8f379041601fa9585620c98a97d))
