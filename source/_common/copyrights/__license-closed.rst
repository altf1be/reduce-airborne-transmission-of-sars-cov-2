.. "= - # ` : ' " ~ ^ _ * < >"

License
===========

.. include:: _header_copyrights.rst

|copy| 2021 Abdelkrim Boujraf, ALT-F1 SPRL <http://www.alt-f1.be>, Luc Cauwenbergh, ProMCF, Carl Fransman, Aconda <https://www.acondasystems.com>

.. include:: _license-trademarks.rst
