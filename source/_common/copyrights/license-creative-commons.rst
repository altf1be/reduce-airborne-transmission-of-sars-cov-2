.. "= - # ` : ' " ~ ^ _ * < >"

License
=============

.. include:: _header_copyrights.rst

|copy| 2010-2020 Abdelkrim Boujraf, alt-f1 sprl <http://www.alt-f1.be>, Abdelkrim Boujraf <http://www.alt-f1.be/literature.html>, Luc Cauwenbergh, ProMCF BVBA, Carl Fransman, Aconda BVBA <https://www.acondasystems.com>

This work is licensed under a Creative Commons Attribution 4.0 International License: <http://creativecommons.org/licenses/by/4.0>

---

You are free to <https://creativecommons.org/licenses/by/4.0/deed.en>:

* Share — copy and redistribute the material in any medium or format 
* Adapt — remix, transform, and build upon the material for any purpose, even commercially
* The licensor cannot revoke these freedoms as long as you follow the license terms.

---

Vous êtes autorisé à <https://creativecommons.org/licenses/by/4.0/deed.fr>: 

* Partager : copier, distribuer et communiquer le matériel par tous moyens et sous tous formats
* Adapter : remixer, transformer et créer à partir du matériel pour toute utilisation, y compris commerciale
* L'Offrant ne peut retirer les autorisations concédées par la licence tant que vous appliquez les termes de cette licence.

---

Je bent vrij om <https://creativecommons.org/licenses/by/4.0/deed.nl>:

* het werk te delen — te kopiëren, te verspreiden en door te geven via elk medium of bestandsformaat
* het werk te bewerken — te remixen, te veranderen en afgeleide werken te maken voor alle doeleinden, inclusief commerciële doeleinden.
* De licentiegever kan deze toestemming niet intrekken zolang aan de licentievoorwaarden voldaan wordt.

.. include:: _license-trademarks.rst
