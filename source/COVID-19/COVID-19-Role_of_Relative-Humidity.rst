.. =  - # ` : ' " ~ ^ _ *  < >

The Role Of Relative Humidity In Airborne Transmission Of Sars-Cov-2 In Indoor Conditions
-----------------------------------------------------------------------------------------

The fate of Microorganisms inside the Viral Droplets
####################################################

A recent study :cite:`Ahlawat_2020` explained that the **viruses survived well at Relative Humidities below 33% and at 100%**, whereas, **at the intermediate Relative Humidities the viability was considerably reduced**

Based on the Lin and Marr (2020) :cite:`doi:10.1021/acs.est.9b04959` findings, **the viability of the virus is typically much lower at a Relative Humidity around 60% (~55%)**.

* Relative Humidity is a factor responsible for airborne transmission of the SARS-CoV-2 virus.
* In dry indoor areas, the chances of airborne transmission are higher than in humid areas.
* Indoor air at 40 to 60% Relative Humidity is the optimum level for human health.
* Important to set minimum Relative Humidity standard for indoor environments.

Survival or Inactivation of Virus on Surfaces (SARS-CoV-1)
##########################################################

A report :cite:`YAO2020139178` based on humidity's role on virus survival and inactivation on surfaces showed that **high temperature at high relative humidity has a collegial impact on inactivation of SARS-CoV-1 viability** (Chan et al., 2011). Whereas **lower temperatures and low humidity support prolonged survival of virus on contaminated surfaces**.

The Role of Dry Indoor Air in Airborne Transmission of Viruses
##############################################################

During cold winters, outdoor air is drawn indoors and then heated to a comfortable temperature level. This process will significantly lower the indoor RH, which creates an extremely dangerous situation for indoor residents, particularly during the COVID-19 pandemic.

**When the indoor RH is less than 40 percent, humans become more vulnerable to viral respiratory infections** making the SARS-CoV-2 virus more infectious in the inhaled air 

During the inhalation of low :term:`RH` air, the mucus in our nose and throat becomes dry and more viscous, which diminishes cilia's capability to expel viral aerosols.

Comparison Of Indoor And Outdoor (Ambient) RH Relationship With Covid-19
------------------------------------------------------------------------------------

Based on an indoor experiment from Chinese cities during Jan-March 2020, **the airborne spread of SARS-CoV-2 was reduced by increasing RH from 23.33% to 82.67%** (Yao et al., 2020)

Feng et al. (2020) :cite:`FENG2020105585` recently investigated the influence of :term:`RH` using numerical modeling. In the study, **they considered 40% RH as lower bound and 95% RH as upper bound.**

They found that 40% :term:`RH` activates the evaporation of water in the cough droplets, leading to droplet shrinkage and prolonged suspension in the air, whereas high :term:`RH` at 95% will increase the droplet size due to hygroscopic growth with higher deposition fractions both on humans and on the ground.

Because, in more humid outdoor environments [i.e., In Malaysia, Singapore], the population is more likely to use drier indoor air and thus promote more COVID viability.

Facility Management Web Platform
--------------------------------

FM Tech develops and maintains the FM Platform: `<https://fmplatform.azurewebsites.net>`_

The web platform triggers alerts and informs the cleaner agent and facility managers of the required tasks to keep a high level of services towards their customers (:term:`Outcome-based contracts`).

The web platform is connected to multiple devices: vacuum cleaners, BACnet-enabled devices (CO2-Air quality, Relative Humidity, Occupancy, Temperature, Light & Sound Intensity, Movement, Noise level), Coffee Machines, Dispensers, Lockers, Office documents, ERPs, Service Management software, Weather forecast, UV, etc.

The FM Platform runs on multiple devices: browser, tablet, or mobile device. The FM Platform runs in the public cloud, in the private cloud, or on-premise.

FM Platform: Dashboard alerting the manager when actions are required
######################################################################

.. image:: img/fmplatform-01-dashboard.png
    :alt: FM Platform : Dashboard alerting the manager when actions are required

Dangerous condition due to Airborne Transmission of SARS-CoV-2 in Indoor Environments
###########################################################################################

.. image:: img/fmplatform-09-role-relative-humidity-covid-19.png
    :alt: The Role of Relative Humidity in Airborne Transmission of SARS-CoV-2 in Indoor Environments

Healthy condition in an indoor environment
##########################################

.. image:: img/fmplatform-10-relative-humidity-green.png
    :alt: Healthy condition in an indoor environment
