Glossary for the FM-Platform
############################

.. glossary::
    :sorted:
   
    FM Platform
        A web platform enabling Agents and Facility Managers to Predict, Validate, and Control the Soft and Hard FM. 
        
        The platform is available: `https://fmplatform.azurewebsites.net <https://fmplatform.azurewebsites.net/?utm_source=documentation&utm_medium=link&utm_campaign=find_partners_manufacture_robot>`_

    Outcome-based contracts
    OBC
    Outcome-based contract
        *"Outcome-based contracting, or its narrower equivalent of performance-based contracting, is a contracting mechanism that allows the customer to pay only when the firm has delivered outcomes, rather than merely activities and tasks"* :cite:`RePEc:eee:eurman:v:27:y:2009:i:6:p:377-387` 

        *"the idea of contracting on outcomes in B2B service contracts is increasingly possible. This is the case for Rolls Royce “Power-by-the-hour®” contracting for the service and support of their engines, where the continuous maintenance and servicing of the engine is not paid according to the spares, repairs or activities rendered to the customer, but by how many hours the customer obtains power from the engine."* :cite:`RePEc:eee:eurman:v:27:y:2009:i:6:p:377-387`

    TSR
    The Smarter Robot
        Vacuum cleaner/mopping Robot. The Robot works autonomously and in cooperation with other robots as a swarm. TSR' clean vast area such as sports hall.

        The Robot is a joint contribution of a `Hardware manufacturer`, `End users` and `FM Tech`

    WAI-ARIA
    Web Accessibility Initiative-Accessible Rich Internet Applications
        WAI-ARIA, the Accessible Rich Internet Applications Suite, defines a way to make Web content and Web applications more accessible to people with disabilities.

        Source: https://www.w3.org/WAI/standards-guidelines/aria/

    RH
        Relative Humidity

        * https://www.sjsu.edu/faculty/watkins/clausius.htm
        * Calculating relative humidity : https://www.theweatherprediction.com/habyhints/186/
        * See Coronavirus Infections—More Than Just the Common Cold :cite:`Paules2020`

