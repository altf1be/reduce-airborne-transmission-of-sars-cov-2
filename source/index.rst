.. Reduce - project name documentation master file, created by
   sphinx-quickstart on Sat Jan 30 13:37:50 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Reduce the Airborne Transmission of Sars-Cov-2
==============================================

.. toctree::
   :maxdepth: 2
   :caption: Table of contents:

   COVID-19/COVID-19-Role_of_Relative-Humidity.rst
   _glossary/FM-Platform/Glossary.rst
   _common/Bibliography.rst
   _common/copyrights/license-creative-commons.rst
  
.. only:: html

    Indices and tables
    ==================

    * :ref:`genindex`
    * :ref:`search`
