# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
    * `npm install`
    * `conda activate sphinx`
* Configuration
* Dependencies
    * 'sphinx.ext.autodoc',
    * 'sphinx.ext.doctest',
    * 'sphinx.ext.intersphinx',
    * 'sphinx.ext.todo',
    * 'sphinx.ext.coverage',
    * 'sphinx.ext.imgmath',
    * 'sphinx.ext.ifconfig',
    * 'sphinx.ext.viewcode',
    * 'sphinx.ext.githubpages',
    * 'sphinx.ext.graphviz',
    * 'sphinx_rtd_theme',
    * 'sphinxcontrib.bibtex',
    * 'docxbuilder'
* Database configuration
    * None
* How to run tests
* Deployment instructions
    * `make html`
        * `xdg-open build/html/index.html`
    * `make latexpdf`
        * `xdg-open build/docx/[filename].pdf`
    * `make docx`
        * `xdg-open build/docx/[filename].docx`
    

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact